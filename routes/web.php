<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@homepage')->name('homepage');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::get('/shop', 'HomeController@shop')->name('shop');
Route::get('/produk-detail/{id}', 'HomeController@productDetail')->name('productDetail');
Route::get('/login-register', 'HomeController@userLoginRegister')->name('login-register');
Auth::routes();

Route::get('/admin/logout', function () {
    Auth::logout();
    return redirect('/login');
});


Route::middleware(['auth', 'user'])->group(function () {
    Route::get('/user/dashboard', 'UserController@userDashboard')->name('userDashboard');
    Route::get('/user/transaksi/{id}', 'UserController@userTransaksi')->name('userTransaksi');
    Route::post('/user/transaksi/{id}/change', 'UserController@userTransaksiChange')->name('userTransaksiChange');
    Route::post('/user/transaksi/{id}/rating', 'UserController@userTransaksiRating')->name('userTransaksiRating');
    Route::get('/user/cart', 'CartController@userCart')->name('userCart');
    Route::get('/user/checkout', 'CartController@checkout')->name('checkout');
    Route::post('/user/checkout/save', 'CartController@checkoutSave')->name('checkoutSave');
    Route::resource('/cart', CartController::class);
    Route::resource('/user/user', UserController::class);

});

Route::middleware('auth', 'admin')->group(function () {
    Route::get('/admin/dashboard', 'HomeController@dashboardAdmin')->name('dashboardAdmin');
    Route::resource('/admin/user', UserController::class);
    Route::resource('/admin/produk', ProdukController::class);
    Route::resource('/admin/kategori', KategoriController::class);
    Route::resource('/admin/merk', MerkController::class);
    Route::resource('/admin/transaksi', TransaksiController::class);
    Route::get('/transaksi-export', 'HomeController@exportExcel')->name('transaksi.export');
});
