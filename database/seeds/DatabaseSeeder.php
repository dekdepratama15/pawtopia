<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'superadmin',
            'email' => 'superadmin@gmail.com',
            'password' => bcrypt('password123'),
            'role' => 'admin',
            'telp' => '6288987406311',
            'alamat' => 'Jalan Raya Semer',
            'img_profile' => 'Jalan Raya Semer',
        ]);
    }
}
