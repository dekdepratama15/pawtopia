<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function uploadGambar($file)
    {
        $newName = '';
        $extension  = $file->getClientOriginalExtension();
        if (in_array($extension, ['jpg', 'png', 'jpeg', 'gif',])) {
            $newName    = rand(0, 999) . now()->timestamp . '.' . $extension;
            $file->move('upload', $newName);
        }
        return $newName;
    }
}
