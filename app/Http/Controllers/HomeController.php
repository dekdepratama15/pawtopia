<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Exports\TransaksiExport;
use App\Produk;
use App\Merk;
use App\Kategori;
use App\Transaksi;
use App\User;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboardAdmin()
    {
        $transaksi_all = Transaksi::whereYear('created_at',date('Y'))->get();
        $transaksi = Transaksi::orderBy('created_at', 'DESC')->take(5)->get();
        $transaksi_done = Transaksi::where('status', 'done')->get();
        $user = User::where('role', 'user')->get();
        $produk = Produk::all();
        $sisa_produk = Produk::where('stok', '<=', 2)->get();
        $total_order = 0;
        $groupedData = $transaksi_all->groupBy(function($date) {
            return \Carbon\Carbon::parse($date->created_at)->format('m');
        });
        foreach ($transaksi_done as $key => $value) {
            $total_order += $value->total;
        }
        return view('admin.dashboard', [
            'transaksi' => $transaksi,
            'transaksi_done' => $transaksi_done,
            'user' => $user,
            'produk' => $produk,
            'total_order' => $total_order,
            'sisa_produk' => $sisa_produk,
            'groupedData' => json_encode($groupedData)
        ]);
    }

    function exportExcel() 
    {
        $transaksi = Transaksi::whereYear('created_at',date('Y'))->get();
        return Excel::download(new TransaksiExport($transaksi), 'transaksi.xlsx');
    }

    public function homepage()
    {
        $bestsellers = Produk::withCount('transaksi_details')->latest('transaksi_details_count')->take(6)->get();
        $merks = Merk::all();

        return view('homepage', [
            'bestsellers' => $bestsellers,
            'merks'       => $merks,
        ]);
    }

    public function contact()
    {
        return view('contact');
    }

    public function shop(Request $request)
    {
        $kategori = 0;
        $search = isset($request->search) && !empty($request->search) ? $request->search : '';
        if (isset($request->kategori) && $request->kategori != 0) {
            $kategori = $request->kategori;
            $produks = Produk::where('nama_produk', 'like', '%'.$search.'%')->where('kategori_id', $request->kategori)->paginate(9);
        } else {
            $produks = Produk::where('nama_produk', 'like', '%'.$search.'%')->paginate(9);
        }
        $kategoris = Kategori::all();
        $form = [
            'search' => $search,
            'kategori' => $kategori,
        ];

        return view('shop', [
            'produks' => $produks,
            'kategoris' => $kategoris,
            'form' => $form,
        ]);
    }

    public function productDetail($id)
    {
        $produk = Produk::find($id);
        $review = $produk->transaksi_details()->where('status_rating', 'done')->get();
        $order = $produk->transaksi_details()->get();
        $related = $produk->kategori->produks()->where('id', '!=', $id)->take(4)->get();
        return view('product_detail', [
            'produk' => $produk,
            'review' => count($review),
            'order' => count($order),
            'related' => $related
        ]);
    }

    public function userLoginRegister(Request $request)
    {
        if (auth()->user() == null) {
            $page = 'login';
            if (isset($request->page) && !empty($request->page)) {
                $page = $request->page;
            }
            return view('login_register', [
                'page' => $page
            ]);
        } else {
            return redirect()->route('userDashboard');
        }
    }
}
