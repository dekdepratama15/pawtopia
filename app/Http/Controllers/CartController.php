<?php

namespace App\Http\Controllers;

use App\Cart;
use App\User;
use App\Produk;
use App\Transaksi;
use App\TransaksiDetail;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cek = Cart::where('user_id', auth()->user()->id)->where('produk_id', $request->id)->get();
        if (count($cek) == 0) {
            $cart = new Cart();
            $cart->user_id = auth()->user()->id;
            $cart->produk_id = $request->id;
            $cart->qty = 1;
            $cart->save();
            return response()->json([
                'success' => true,
                'message' => 'Data berhasil ditambahkan ke Keranjang!'
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Data sudah ada di dalam Keranjang!'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function show(Cart $cart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function edit(Cart $cart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cart $cart)
    {
        if (is_numeric($request->qty)) {
            $cart = Cart::find($request->id);
            if ($cart->produk->stok >= $request->qty) {
                $cart->qty = $request->qty;
                $cart->save();
    
                return response()->json([
                    'success' => true,
                    'message' => 'Data keranjang berhasil diubah!'
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Stok produk tidak cukup!'
                ]);
            }
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Data yang diinputkan harus berupa angka!'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cart $cart)
    {
        Cart::destroy($cart->id);
        return response()->json(['message' => 'Data Berhasil Dihapus!']);
    }

    public function userCart()
    {
        if (auth()->user() == null) {
            return [];
        } else {
            $cart = [];
            foreach (auth()->user()->carts as $key => $value) {
                $value->produk = $value->produk;
                $cart[] = $value;
            }
            return $cart;
        }
    }

    public function checkout()
    {
        $user = User::find(auth()->user()->id);
        return view('user.checkout', [
            'user' => $user
        ]);
    }

    public function checkoutSave(Request $request)
    {
        $user = User::find(auth()->user()->id);
        $transaksi = new Transaksi();
        $transaksi->total = $request->total;
        $transaksi->user_id = $user->id;
        $transaksi->save();

        foreach ($user->carts as $key => $value) {
            $transaksi_detail = new TransaksiDetail();
            $transaksi_detail->transaksi_id = $transaksi->id;
            $transaksi_detail->produk_id = $value->produk_id;
            $transaksi_detail->harga_beli = $value->produk->harga;
            $transaksi_detail->qty = $value->qty;
            $transaksi_detail->save();

            $value->produk->stok = intval($value->produk->stok) - intval($value->qty);
            $value->produk->save();
        }
        $user->carts()->delete();
        session()->flash('success', 'Berhasil melakukan transaksi!');

        return redirect('/user/dashboard');

    }
}
