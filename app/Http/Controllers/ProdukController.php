<?php

namespace App\Http\Controllers;

use App\Kategori;
use App\Merk;
use App\Produk;
use Illuminate\Http\Request;

class ProdukController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk = Produk::all();
        return view('admin.produk.index', compact('produk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::all();
        $merk = Merk::all();
        return view('admin.produk.create', ['kategori' => $kategori, 'merk' => $merk]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $produk = new Produk();
        $produk->nama_produk = $request->nama_produk;
        $produk->deskripsi = $request->deskripsi;
        $produk->harga = $request->harga;
        $produk->stok = $request->stok;
        $gambar_produk = '';
        if (isset($request->gambar)) {
            $gambar_produk = $this->uploadGambar($request->gambar);
        }
        $produk->gambar = $gambar_produk;
        $produk->kategori_id = $request->kategori;
        $produk->merk_id = $request->merk;

        $produk->save();

        session()->flash('success', 'Data Produk Berhasil ditambahkan!');
        return redirect('/admin/produk');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function show(Produk $produk)
    {
        $kategori = Kategori::all();
        $merk = Merk::all();
        return view('admin.produk.show', ['produk' => $produk, 'kategori' => $kategori, 'merk' => $merk]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function edit(Produk $produk)
    {
        $kategori = Kategori::all();
        $merk = Merk::all();
        return view('admin.produk.edit', ['produk' => $produk, 'kategori' => $kategori, 'merk' => $merk]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Produk $produk)
    {
        $produk->nama_produk = $request->nama_produk;
        $produk->deskripsi = $request->deskripsi;
        $produk->harga = $request->harga;
        $produk->stok = $request->stok;
        if (isset($request->gambar)) {
            $gambar_produk = $this->uploadGambar($request->gambar);
            $produk->gambar = $gambar_produk;
        }
        $produk->kategori_id = $request->kategori;
        $produk->merk_id = $request->merk;

        $produk->save();

        session()->flash('success', 'Data Produk Berhasil Diubah!');
        return redirect('/admin/produk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function destroy(Produk $produk)
    {
        Produk::destroy($produk->id);
        return response()->json(['message' => 'Data Berhasil Dihapus!']);
    }
}
