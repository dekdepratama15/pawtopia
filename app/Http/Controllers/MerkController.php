<?php

namespace App\Http\Controllers;

use App\Merk;
use Illuminate\Http\Request;

class MerkController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $merk = Merk::all();
        return view('admin.merk.index', compact('merk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.merk.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $merek = new Merk();
        $merek->nama_merk = $request->nama_merk;
        $merekgambar = '';
        if (isset($request->gambar)) {
            $merekgambar = $this->uploadGambar($request->gambar);
        }
        $merek->gambar = $merekgambar;
        $merek->save();

        session()->flash('success', 'Berhasil Menambahkan Data Merek!');

        return redirect('/admin/merk');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Merk  $merk
     * @return \Illuminate\Http\Response
     */
    public function show(Merk $merk)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Merk  $merk
     * @return \Illuminate\Http\Response
     */
    public function edit(Merk $merk)
    {
        return view('admin.merk.edit', compact('merk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Merk  $merk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Merk $merk)
    {
        $merk->nama_merk = $request->nama_merk;
        if (isset($request->gambar)) {
            $merekgambar = $this->uploadGambar($request->gambar);
            $merk->gambar = $merekgambar;
        }
        $merk->save();

        session()->flash('success', 'Berhasil Menambahkan Data Merek!');

        return redirect('/admin/merk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Merk  $merk
     * @return \Illuminate\Http\Response
     */
    public function destroy(Merk $merk)
    {
        Merk::destroy($merk->id);
        return response()->json(['message' => 'Data Berhasil Dihapus!']);
    }
}
