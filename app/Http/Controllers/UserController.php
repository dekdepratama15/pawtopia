<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Transaksi;
use App\TransaksiDetail;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('role', 'user')->get();
        return view('admin.user.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->telp = $request->telp;
        $user->alamat = $request->alamat;
        if (!empty($request->new_password) && !empty($request->confirm_password)) {
            $user->password = bcrypt($request->new_password);
        }
        $user->save();
        session()->flash('success', 'Berhasil mengubah data!');

        return redirect('/user/dashboard');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return response()->json(['message' => 'Data Berhasil Dihapus!']);
    }

    public function userDashboard()
    {
        $user = User::find(auth()->user()->id);
        return view('user.dashboard', [
            'user' => $user
        ]);
    }

    public function userTransaksi($id)
    {
        $transaksi = Transaksi::find($id);
        return view('user.transaksi.show', [
            'transaksi' => $transaksi
        ]);
    }

    public function userTransaksiChange(Request $request, $id)
    {
        $transaksi = Transaksi::find($id);
        if (isset($request->payment_proof)) {
            $transaksi->bukti_bayar = $this->uploadGambar($request->payment_proof);
            $transaksi->tgl_bayar = date('Y-m-d H:i:s');
            $transaksi->status = 'payment_send';
            session()->flash('success', 'Bukti bayar berhasil dikirim!');
        } else {
            $transaksi->status = 'received';
            session()->flash('success', 'Status telah diubah!');
        }
        $transaksi->save();

        return redirect('/user/transaksi/'.$id);
    }

    public function userTransaksiRating(Request $request, $id)
    {
        $transaksi = Transaksi::find($id);
        foreach ($transaksi->transaksi_details as $key => $value) {
            $value->rating = $request->rating[$key];
            $value->deskripsi_rating = $request->deskripsi[$key];
            $value->status_rating = 'done';
            $value->save();

            $produk = $value->produk;
            $produk_transaksi_details = $produk->transaksi_details()->where('status_rating', 'done')->get();
            $rating_produk = 0;
            foreach ($produk_transaksi_details as $k => $v) {
                $rating_produk += $v->rating;
            }
            $rating_produk = ($rating_produk / count($produk_transaksi_details));
            $produk->rating = $rating_produk;
            $produk->save();

        }
        $transaksi->status = 'done';
        $transaksi->save();
        session()->flash('success', 'Data Rating berhasil dikirim!');

        return redirect('/user/transaksi/'.$id);
    }
}
