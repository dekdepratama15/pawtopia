<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function transaksi_details()
    {
        return $this->hasMany(TransaksiDetail::class);
    }
}
