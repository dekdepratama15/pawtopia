<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    public function produks()
    {
        return $this->hasMany(Produk::class);
    }
}
