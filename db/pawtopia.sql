-- -------------------------------------------------------------
-- TablePlus 5.3.0(486)
--
-- https://tableplus.com/
--
-- Database: pawtopia
-- Generation Time: 2024-01-05 20:55:19.4860
-- -------------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


DROP TABLE IF EXISTS `carts`;
CREATE TABLE `carts` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `produk_id` bigint unsigned NOT NULL,
  `qty` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `carts_user_id_foreign` (`user_id`),
  KEY `carts_produk_id_foreign` (`produk_id`),
  CONSTRAINT `carts_produk_id_foreign` FOREIGN KEY (`produk_id`) REFERENCES `produks` (`id`) ON DELETE CASCADE,
  CONSTRAINT `carts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `kategoris`;
CREATE TABLE `kategoris` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `merks`;
CREATE TABLE `merks` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nama_merk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `gambar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `produks`;
CREATE TABLE `produks` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `kategori_id` bigint unsigned NOT NULL,
  `merk_id` bigint unsigned NOT NULL,
  `nama_produk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stok` int NOT NULL,
  `gambar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `produks_kategori_id_foreign` (`kategori_id`),
  KEY `produks_merk_id_foreign` (`merk_id`),
  CONSTRAINT `produks_kategori_id_foreign` FOREIGN KEY (`kategori_id`) REFERENCES `kategoris` (`id`) ON DELETE CASCADE,
  CONSTRAINT `produks_merk_id_foreign` FOREIGN KEY (`merk_id`) REFERENCES `merks` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `transaksi_details`;
CREATE TABLE `transaksi_details` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `transaksi_id` bigint unsigned NOT NULL,
  `produk_id` bigint unsigned NOT NULL,
  `harga_beli` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int NOT NULL,
  `status_rating` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `deskripsi_rating` text COLLATE utf8mb4_unicode_ci,
  `rating` int NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transaksi_details_transaksi_id_foreign` (`transaksi_id`),
  KEY `transaksi_details_produk_id_foreign` (`produk_id`),
  CONSTRAINT `transaksi_details_produk_id_foreign` FOREIGN KEY (`produk_id`) REFERENCES `produks` (`id`) ON DELETE CASCADE,
  CONSTRAINT `transaksi_details_transaksi_id_foreign` FOREIGN KEY (`transaksi_id`) REFERENCES `transaksis` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `transaksis`;
CREATE TABLE `transaksis` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `total` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_bayar` datetime DEFAULT NULL,
  `bukti_bayar` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transaksis_user_id_foreign` (`user_id`),
  CONSTRAINT `transaksis_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci,
  `img_profile` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `kategoris` (`id`, `nama_kategori`, `created_at`, `updated_at`) VALUES
(1, 'Makanan Anjing', '2024-01-03 15:24:50', '2024-01-03 15:24:50'),
(2, 'Makanan Kucing', '2024-01-03 15:25:00', '2024-01-03 15:25:00');

INSERT INTO `merks` (`id`, `nama_merk`, `created_at`, `updated_at`, `gambar`) VALUES
(2, 'Pedigree', '2024-01-02 18:55:52', '2024-01-03 19:27:11', '9891704310031.png'),
(3, 'Bolt', '2024-01-03 15:04:22', '2024-01-03 15:04:22', '8791704294262.png');

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2023_06_08_114544_create_kategoris_table', 1),
(4, '2023_06_08_114641_create_merks_table', 1),
(5, '2023_06_08_115003_create_produks_table', 1),
(6, '2023_06_08_115433_create_transaksis_table', 1),
(7, '2023_06_08_120714_create_transaksi_details_table', 1),
(8, '2023_06_10_192626_update_table_merk', 1),
(9, '2023_06_14_040556_create_carts_table', 1);

INSERT INTO `produks` (`id`, `kategori_id`, `merk_id`, `nama_produk`, `deskripsi`, `harga`, `stok`, `gambar`, `rating`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 'Pedigree Junior Chicken 1.15kg ( Kaleng ) – Makanan Basah Anjing', 'PEDIGREE Nutrisi Lengkap Dewasa untuk Anjing telah diformulasikan ulang untuk memberikan energi dan nutrisi yang dibutuhkan anjing untuk melanjutkan kehidupan semaksimal mungkin. Seng dan asam linoleat dalam resep ini membantu menjaga anjing dewasa terlihat seperti pemenang terbaik dalam pertunjukan, dan kadar vitamin E & C antioksidan terkemuka bekerja dengan sistem kekebalan tubuh mereka untuk membuat mereka merasa seperti anak anjing. Perpaduan serat khusus kami juga membantu menjaga perut mereka bahagia dan patroli hal juga mudah! Plus, milik kita', '65000', 99, '4971704302417.jpeg', '4', '2024-01-03 17:20:17', '2024-01-04 21:50:01'),
(2, 1, 2, 'Pedigree Adult Lamb & Veg (3 kg) – Makanan Kering Anjing', '100% Makanan lengkap dan seimbang yang dibuat dengan potongan daging lembab dan bahan-bahan khusus untuk memberinya vitalitas yang sehat dan makanan yang akan dicintainya setiap hari.\r\n– Potongan gemuk lembab memberikan protein berkualitas tinggi untuk otot yang kuat.\r\n– Serat prebiotik alami untuk kesehatan usus yang baik.\r\n– Sayuran yang dipilih untuk mineral dan vitamin.\r\n– Potongan renyah yang dirancang khusus untuk gusi dan gigi yang sehat.\r\n– Diperkaya dengan Omega 6 untuk kulit yang sehat dan mantel yang mengilap.', '135000', 78, '3451704302779.png', '3', '2024-01-03 17:26:19', '2024-01-04 21:03:25'),
(3, 2, 3, 'Bolt Cat Food Kitten 20kg – Makanan Anak Kucing', 'BOLT KITTEN 20KG RASA SALMON\r\n\r\n– untuk pertumbuhan tulang dan gigi kuat\r\n– Meningkatkan sistem imunitas\r\n– Kulit Sehat dan bulu berkilau\r\n– Penglihatan tajam\r\n– Mengurangi resiko FLUTD', '495000', 119, '4291704302932.jpg', '5', '2024-01-03 17:28:52', '2024-01-04 21:03:25'),
(4, 2, 3, 'Bolt Cat Food Kitten 20kg – Makanan Anak Kucing', 'BOLT KITTEN 20KG RASA SALMON\r\n\r\n– untuk pertumbuhan tulang dan gigi kuat\r\n– Meningkatkan sistem imunitas\r\n– Kulit Sehat dan bulu berkilau\r\n– Penglihatan tajam\r\n– Mengurangi resiko FLUTD', '495000', 120, '4291704302932.jpg', NULL, '2024-01-03 17:28:52', '2024-01-03 17:28:52');

INSERT INTO `transaksi_details` (`id`, `transaksi_id`, `produk_id`, `harga_beli`, `qty`, `status_rating`, `deskripsi_rating`, `rating`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '135000', 2, 'done', 'cukup', 3, '2024-01-04 20:36:13', '2024-01-04 21:03:25'),
(2, 1, 3, '495000', 1, 'done', 'baik sekali', 5, '2024-01-04 20:36:13', '2024-01-04 21:03:25'),
(3, 2, 1, '65000', 1, 'done', 'Sesuai', 4, '2024-01-04 21:46:22', '2024-01-04 21:50:01');

INSERT INTO `transaksis` (`id`, `user_id`, `status`, `total`, `tgl_bayar`, `bukti_bayar`, `created_at`, `updated_at`) VALUES
(1, 2, 'done', '765000', '2024-01-04 20:53:50', '1681704401630.jpg', '2024-01-04 20:36:13', '2024-01-04 21:03:25'),
(2, 2, 'done', '65000', '2024-01-04 21:48:12', '8061704404892.png', '2024-01-04 21:46:22', '2024-01-04 21:50:01'),
(3, 2, 'pending', '65000', '2024-01-04 21:48:12', '8061704404892.png', '2024-02-04 21:46:22', '2024-01-04 21:50:01');

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `role`, `telp`, `alamat`, `img_profile`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'superadmin@gmail.com', NULL, '$2y$10$LCoC7PraijvaFtrWJVI4.OWN7jxOYhQ8hpBhVAWB/FsoL1I/nI7/G', NULL, 'admin', '6288987406311', 'Jalan Raya Semer', 'Jalan Raya Semer', NULL, NULL),
(2, 'Andika Ary Pratama', 'andikaary99@gmail.com', NULL, '$2y$10$3AqJhg/3TSMuW4XU6T00aup2REVSI2q2uZskSIqlBYTfuZ7vXs5YW', NULL, 'user', '088987406311', 'Jln Raya Semer Gg. Tunjung Mekar No. 49, Kerobokan, Kuta Utara', 'user-no-image.png', '2024-01-03 17:50:06', '2024-01-04 11:49:09'),
(3, 'I Gede Andika Ary Pratama', 'andikaary998@gmail.com', NULL, '$2y$10$VW0TcUzAHPg6xQCUFQaSjuBRMM5MXNJXU4ITB1DQ7SUSn5NFMttba', NULL, 'user', '088987406311', 'Jalan Raya Semer', 'user-no-image.png', '2024-01-04 11:07:51', '2024-01-04 11:07:51');



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;