<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Pawtopia</title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon -->
    <link rel="icon" href="{{url('/logo/1.png')}}">

    <!-- All CSS is here
	============================================ -->
   <link rel="stylesheet" type="text/css" href="{{ asset('user/css/bootstrap.min.css') }}">
   <link rel="stylesheet" href="{{ asset('user/css/owl.carousel.min.css') }}">
   <link rel="stylesheet" href="{{ asset('user/css/owl.theme.default.min.css') }}">
   <link rel="stylesheet" href="{{ asset('user/css/slick.css') }}">
   <link rel="stylesheet" href="{{ asset('user/css/slick-theme.css') }}">
   <link rel="stylesheet" href="{{ asset('user/css/animate.min.css') }}">
   <!-- nice-select -->
   <link rel="stylesheet" href="{{ asset('user/css/nice-select.css') }}">
   <!-- fancybox -->
   <link rel="stylesheet" href="{{ asset('user/css/jquery.fancybox.min.css') }}">
   <link rel="stylesheet" href="{{ asset('user/css/fontawesome.min.css') }}">
   <!-- style -->
   <link rel="stylesheet" href="{{ asset('user/css/style.css') }}">
   <!-- responsive -->
   <link rel="stylesheet" href="{{ asset('user/css/responsive.css') }}">
   <!-- color -->
   <link rel="stylesheet" href="{{ asset('user/css/color.css') }}">
   <!-- jQuery -->
   <script src="{{ asset('user/js/jquery-3.6.0.min.js') }}"></script>
   <script src="{{ asset('user/js/preloader.js') }}"></script>
    <style>
        .swal2-container {
            z-index: 9999; 
        }
        .swal2-input{
            width: 450px !important;
        }
        .cart-number {
            position: absolute;
            top: -80%;
            right: -80%;
            font-size: 10px;
            color: #fff;
            background: black;
            padding: 5px 10px;
            border-radius: 100%;
        }
    </style>
</head>

<body>
    <!-- loader -->
    <div class="preloader"> 
        <div class="container">
        <div class="dot dot-1"></div>
        <div class="dot dot-2"></div>
        <div class="dot dot-3"></div>
        </div>
    </div>
    <!-- loader end -->

    <div class="main-wrapper">
        @include('layouts.user.navbar')
        
        @yield('content')
        
        @include('layouts.user.footer')
        @include('layouts.user.sidebar')
    </div>

    <!-- All JS is here
============================================ -->

    
    <!-- progress -->
    <div id="progress">
        <span id="progress-value"><i class="fa-solid fa-up-long"></i></span>
    </div>
    <!-- progress end -->
    <!-- Bootstrap Js -->
    <script src="{{ asset('user/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('user/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('user/js/slick.min.js') }}"></script>
    <script src="{{ asset('user/js/jquery.nice-select.min.js') }}"></script>
    <!-- fancybox -->
    <script src="{{ asset('user/js/jquery.fancybox.min.js') }}"></script>
    <script src="{{ asset('user/js/custom.js') }}"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    
    @if(session('success'))
        <script>
        $(document).ready(function() {
            Swal.fire(
                'Berhasil!',
                '{{session("success")}}',
                'success'
            );
        });
        </script>
    @endif

    @if(session('warning'))
        <script>
        $(document).ready(function() {
            Swal.fire(
                'Perhatian!',
                '{{session("warning")}}',
                'warning'
            );
        });
        </script>
    @endif
    
    <script>
        $(document).ready(function() {
            userCart();
        });

        function setToRp(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
        }

        function userCart() {
            $.ajax({
                type: "get",
                url: '/user/cart',
                success: function (response) {
                    console.log(response)
                    if (response.length == 0) {
                        $('.count-cart').html(parseInt(0));
                        $('#ul-shopping-cart').html('<li>No Item in Cart</li>');
                        $('#sub-total-cart').html('Rp. ' + setToRp(0));
                    } else {
                        var text = '';
                        var subtotal = 0;
                        $.each(response, function (key, val) {
                            subtotal += parseFloat(val.produk.harga) * parseInt(val.qty);
                            text += `
                                <li class="d-flex align-items-center position-relative">

                                    <div class="p-img light-bg">

                                        <img src="{{url('upload/${val.produk.gambar}')}}" alt="Product Image">

                                    </div>

                                    <div class="p-data">

                                        <h3 class="font-semi-bold">${val.produk.nama_produk}</h3>
                                        
                                        <p class="theme-clr font-semi-bold">${setToRp(val.qty)} × Rp. ${setToRp(val.produk.harga)}</p>

                                    </div>

                                </li>
                                <li><button class="btn btn-sm btn-primary update-qty-cart" data-id="${val.id}">Ubah Qty</button> <button class="btn btn-sm btn-danger delete-cart" data-id="${val.id}">Hapus</button></li>
                            `;
                        });
                        $('.count-cart').html(parseInt(response.length));
                        $('#ul-shopping-cart').html(text);
                        $('#sub-total-cart').html('Rp. ' + setToRp(subtotal));
                        deleteCart();
                        updateQtyCart();
                    }
                }
            });
        }

        function updateQtyCart() {
            $('.update-qty-cart').click(function (e) {
                e.preventDefault();
                var id = $(this).data("id");
                var token = $('meta[name=csrf-token]').attr('content');

                Swal.fire({
                title: 'Ubah jumlah Produk',
                input: 'text',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Ubah',
                showLoaderOnConfirm: true,
                preConfirm: (qty) => {
                    $.ajax({
                        type: "PUT",
                        url: '/cart/'+id,
                        data: {
                            "id": id,
                            "_method": 'PUT',
                            "_token": token,
                            'qty':qty
                        },
                        success: function (response) {
                            if (response.success) {
                                userCart();
                            }

                            Swal.fire(
                                response.success ? 'Berhasil!' : 'Perhatian!',
                                response.message,
                                response.success ? 'success' : 'warning',
                            );
                        }
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
                });
            })
        }

        function deleteCart() {
            $('.delete-cart').click(function (e) { 
                e.preventDefault();
                var id = $(this).data("id");
                var token = $('meta[name=csrf-token]').attr('content');
                
                Swal.fire({
                    title: 'Yakin?',
                    text: "Data akan dihapus dari keranjang!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#0275d8',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Hapus!',
                    cancelButtonText: 'Batal'
                })
                .then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "delete",
                            url: '/cart/'+id,
                            data: {
                                "id": id,
                                "_method": 'DELETE',
                                "_token": token,
                            },
                            success: function (response) {
                                Swal.fire(
                                    'Berhasil!',
                                    response.message,
                                    'success'
                                );
                                userCart();
                            }
                        });
                    }
                })
            });
        }
    </script>
    @yield('js-extra')
</body>

</html>