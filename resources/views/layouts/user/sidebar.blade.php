<!-- cart-popup -->
<div id="lightbox" class="lightbox clearfix">
        <div class="white_content">
            <a href="javascript:;" class="textright" id="close"><i class="fa-regular fa-circle-xmark"></i></a>
                <div class="cart-popup">
                <ul id="ul-shopping-cart">

                </ul>

                <div class="cart-total d-flex align-items-center justify-content-between">

                <span class="font-semi-bold">Total:</span>

                <span class="font-semi-bold" id="sub-total-cart">$23.10</span>

                </div>

                <div class="cart-btns d-flex align-items-center justify-content-between">

                <a class="font-bold theme-bg-clr text-white checkout" style="width: 100%;" href="{{url('/user/checkout')}}">Checkout</a>

                </div>
        </div>
        </div>
    </div>
<!-- cart-popup end -->