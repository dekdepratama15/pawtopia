<nav id="sidebar" aria-label="Main Navigation">
    <!-- Side Header -->
    <div class="bg-header-dark">
      <div class="content-header bg-white-5">
        <!-- Logo -->
        <a class="fw-semibold text-white tracking-wide" href="{{route('dashboardAdmin')}}">
          <span class="smini-visible">
            <img src="{{ asset('logo/4.png') }}" alt="">
          </span>
          <span class="smini-hidden mt-3">
            <img src="{{ asset('logo/4.png') }}" alt="" style="width: 150px">
            <!-- Admin Paw<span class="opacity-75">topia</span> -->
          </span>
        </a>
        <!-- END Logo -->

      </div>
    </div>
    <!-- END Side Header -->

    <!-- Sidebar Scrolling -->
    <div class="js-sidebar-scroll">
      <!-- Side Navigation -->
      <div class="content-side">
        <ul class="nav-main">
          <li class="nav-main-item">
            <a class="nav-main-link {{ Request::path() == 'admin/dashboard' ? 'active' : '' }}" href="{{url('/admin/dashboard')}}">
              <i class="nav-main-link-icon fa fa-bars"></i>
              <span class="nav-main-link-name">Dashboard</span>
            </a>
          </li>
          <li class="nav-main-item pt-2">
            <a class="nav-main-link {{ Request::path() == 'admin/user' ? 'active' : '' }}" href="{{url('/admin/user')}}">
              <i class="nav-main-link-icon fa fa-user"></i>
              <span class="nav-main-link-name">User</span>
            </a>
          </li>
          <li class="nav-main-item pt-2">
            <a class="nav-main-link {{ Request::path() == 'admin/merk' ? 'active' : '' }}" href="{{url('/admin/merk')}}">
              <i class="nav-main-link-icon fa fa-tags"></i>
              <span class="nav-main-link-name">Merk</span>
            </a>
          </li>
          <li class="nav-main-item pt-2">
            <a class="nav-main-link {{ Request::path() == 'admin/kategori' ? 'active' : '' }}" href="{{url('/admin/kategori')}}">
              <i class="nav-main-link-icon fa fa-th-list"></i>
              <span class="nav-main-link-name">Kategori</span>
            </a>
          </li>
          <li class="nav-main-item pt-2">
            <a class="nav-main-link {{ Request::path() == 'admin/produk' ? 'active' : '' }} " href="{{url('/admin/produk')}}">
              <i class="nav-main-link-icon fa fa-shopping-cart"></i>
              <span class="nav-main-link-name">Produk</span>
            </a>
          </li>
          <li class="nav-main-item pt-2">
            <a class="nav-main-link {{ Request::path() == 'admin/transaksi' ? 'active' : '' }}" href="{{url('/admin/transaksi')}}">
              <i class="nav-main-link-icon fa fa-usd"></i>
              <span class="nav-main-link-name">Transaksi</span>
            </a>
          </li>
        </ul>
      </div>
      <!-- END Side Navigation -->
    </div>
    <!-- END Sidebar Scrolling -->
  </nav>