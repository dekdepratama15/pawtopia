@extends('layouts.user.app')

@section('content')
<section class="hero-three" style="background-image:url({{ asset('user/img/background-3.jpg') }})">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 item">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="hero-three-text">
                            <h2>Peliharaanmu adalah bagian Keluargamu</h2>
                            <ul class="list">
                                <li><i class="fa-solid fa-check"></i>Nutrisi cukup</li>
                                <li><i class="fa-solid fa-check"></i>Lingkungan bagus</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="hero-three-img">
                            <img src="{{ asset('user/img/all-100.png') }}" class="all-100" alt="all-100">
                            <img src="{{ asset('user/img/bg-dog-2.jpeg') }}" class="hero-three-img" alt="hero-three">
                            <img src="{{ asset('user/img/bg-dog.jpeg') }}" alt="hero-three">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <img src="{{ asset('user/img/haddi.png') }}" alt="hero-shaps" class="img-2">
    <img src="{{ asset('user/img/dabal-foot-2.png') }}" alt="hero-shaps" class="img-3">
    <img src="{{ asset('user/img/line.png') }}" alt="hero-shaps" class="img-4">
</section>
<section class="gap no-top section-healthy-product mt-5">
    <div class="container">
        <div class="heading">
            <img src="{{ asset('user/img/heading-img.png') }}" alt="heading-img">
            <h6>Temukan Produk dengan Merk</h6>
            <h2>Merk yang terdaftar</h2>
        </div>
        <div class="row slider-categorie owl-carousel owl-theme">
            @foreach($merks as $merk)
                <div class="col-lg-12 item">
                    <div class="food-categorie">
                        <img src="{{url('upload/'.$merk->gambar)}}" alt="{{$merk->nama_merk}}" style="width: 150px; height: 150px; object-fit: cover !important;">
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
<section class="gap" style="background-image: url({{ asset('user/img/healthy-product.png') }}); background-color: #f5f5f5;">
    <div class="container">
        <div class="heading">
            <img src="{{ asset('user/img/heading-img.png') }}" alt="heading-img">
            <h6>Temukan Produk Sehat</h6>
            <h2>Produk Bernutrisi</h2>
        </div>
        <div class="row">
            @foreach($bestsellers as $bestseller)
                <div class="col-xl-4 col-md-6">
                    <div class="products">
                        <div class="products-img">
                            <img src="{{ url('upload/'.$bestseller->gambar) }}" style="width: 222px; height: 278px; object-fit: cover !important" alt="products">
                        </div>
                        <span>{{$bestseller->kategori->nama_kategori}}</span>
                        <a href="{{url('/produk-detail/'.$bestseller->id)}}">{{ $bestseller->nama_produk }}</a>
                        <div class="products-text">
                            <h4>Rp. {{ number_format($bestseller->harga, 0, '.', ',') }}</h4>
                            <div class="d-flex align-items-center">
                                <ul class="star">
                                    @php 
                                        $rating = !empty($bestseller->rating) ? $bestseller->rating : 0;
                                    @endphp
                                    @for($i = 1; $i <= 5; $i++)
                                        @if($rating - $i >= 0)
                                            <li><i class="fa-solid fa-star"></i></li>
                                        @elseif($rating - $i < 0 && $rating - $i > -1)
                                            <li><i class="fa-solid fa-star-half-alt"></i></li>
                                        @else
                                            <li><i class="fa-regular fa-star"></i></li>
                                        @endif
                                    @endfor
                                </ul>
                                <div class="cart">
                                    <a type="button" class="btn-add-to-cart" data-id="{{$bestseller->id}}" data-token="{{ csrf_token() }}">
                                        <svg enable-background="new 0 0 512 512" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><g><path d="m452 120h-60.946c-7.945-67.478-65.477-120-135.054-120s-127.109 52.522-135.054 120h-60.946c-11.046 0-20 8.954-20 20v352c0 11.046 8.954 20 20 20h392c11.046 0 20-8.954 20-20v-352c0-11.046-8.954-20-20-20zm-196-80c47.484 0 87.019 34.655 94.659 80h-189.318c7.64-45.345 47.175-80 94.659-80zm176 432h-352v-312h40v60c0 11.046 8.954 20 20 20s20-8.954 20-20v-60h192v60c0 11.046 8.954 20 20 20s20-8.954 20-20v-60h40z"></path></g></svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="btn-center">
                <a href="{{url('/shop')}}" class="button">Lihat Semua</a>
            </div>
    </div>
</section>
<section class="gap">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="heading two">
                    <h6>Berikan peliharaanmu service berkualitas</h6>
                    <h2>Kami menyediakan Pet Care Services.</h2>
                </div>
            </div>
            <div class="offset-lg-1 col-lg-5">
                <p>Belanja Online Kebutuhan Hewan Peliharaan HANYA <b>Pawtopia</b>!, Menyediakan brand lokal dan internasional yang terus bertambah untuk PetLovers di seluruh Indonesia.</p>
            </div>
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-md-6">
                            <div class="pet-grooming">
                            <i><img src="{{ asset('user/img/welcome-to-3.png') }}" alt="icon"></i>
                            <svg width="138" height="138" viewBox="0 0 673 673" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M9.82698 416.603C-19.0352 298.701 18.5108 173.372 107.497 90.7633L110.607 96.5197C24.3117 177.199 -12.311 298.935 15.0502 413.781L9.82698 416.603ZM89.893 565.433C172.674 654.828 298.511 692.463 416.766 663.224L414.077 658.245C298.613 686.363 175.954 649.666 94.9055 562.725L89.893 565.433ZM656.842 259.141C685.039 374.21 648.825 496.492 562.625 577.656L565.413 582.817C654.501 499.935 691.9 374.187 662.536 256.065L656.842 259.141ZM581.945 107.518C499.236 18.8371 373.997 -18.4724 256.228 10.5134L259.436 16.4515C373.888 -10.991 495.248 25.1518 576.04 110.708L581.945 107.518Z" fill="#940c69"></path>
                            </svg>
                            <a href="#"><h4>PET Hotel</h4></a>
                            <p>Melayani Jasa Penitipan Kucing Dan Anjing Harian Dengan Pelayanan Terpercaya</p>
                            </div>
                    </div>
                    <div class="col-md-6">
                            <div class="pet-grooming">
                            <i><img src="{{ asset('user/img/welcome-to-1.png') }}" alt="icon"></i>
                            <svg width="138" height="138" viewBox="0 0 673 673" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M9.82698 416.603C-19.0352 298.701 18.5108 173.372 107.497 90.7633L110.607 96.5197C24.3117 177.199 -12.311 298.935 15.0502 413.781L9.82698 416.603ZM89.893 565.433C172.674 654.828 298.511 692.463 416.766 663.224L414.077 658.245C298.613 686.363 175.954 649.666 94.9055 562.725L89.893 565.433ZM656.842 259.141C685.039 374.21 648.825 496.492 562.625 577.656L565.413 582.817C654.501 499.935 691.9 374.187 662.536 256.065L656.842 259.141ZM581.945 107.518C499.236 18.8371 373.997 -18.4724 256.228 10.5134L259.436 16.4515C373.888 -10.991 495.248 25.1518 576.04 110.708L581.945 107.518Z" fill="#940c69"></path>
                            </svg>
                            <a href="#"><h4>Pet Grooming</h4></a>
                            <p> Melayani Jasa Grooming Kucing & Anjing Kesayangan dan dapatkan FREE VITAMIN</p>
                            </div>
                    </div>
                    <div class="col-md-6">
                            <div class="pet-grooming mt-5">
                            <i><img src="{{ asset('user/img/welcome-to-4.png') }}" alt="icon"></i>
                            <svg width="138" height="138" viewBox="0 0 673 673" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M9.82698 416.603C-19.0352 298.701 18.5108 173.372 107.497 90.7633L110.607 96.5197C24.3117 177.199 -12.311 298.935 15.0502 413.781L9.82698 416.603ZM89.893 565.433C172.674 654.828 298.511 692.463 416.766 663.224L414.077 658.245C298.613 686.363 175.954 649.666 94.9055 562.725L89.893 565.433ZM656.842 259.141C685.039 374.21 648.825 496.492 562.625 577.656L565.413 582.817C654.501 499.935 691.9 374.187 662.536 256.065L656.842 259.141ZM581.945 107.518C499.236 18.8371 373.997 -18.4724 256.228 10.5134L259.436 16.4515C373.888 -10.991 495.248 25.1518 576.04 110.708L581.945 107.518Z" fill="#940c69"></path>
                            </svg>
                            <a href="#"><h4>Pet Training</h4></a>
                            <p>Melayani Jasa Pelatihan Anjing Profesional dan Berkonsultasi secara Gratis.</p>
                            </div>
                    </div>
                    <div class="col-md-6">
                            <div class="pet-grooming mt-5">
                            <i><img src="{{ asset('user/img/welcome-to-2.png') }}" alt="icon"></i>
                            <svg width="138" height="138" viewBox="0 0 673 673" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M9.82698 416.603C-19.0352 298.701 18.5108 173.372 107.497 90.7633L110.607 96.5197C24.3117 177.199 -12.311 298.935 15.0502 413.781L9.82698 416.603ZM89.893 565.433C172.674 654.828 298.511 692.463 416.766 663.224L414.077 658.245C298.613 686.363 175.954 649.666 94.9055 562.725L89.893 565.433ZM656.842 259.141C685.039 374.21 648.825 496.492 562.625 577.656L565.413 582.817C654.501 499.935 691.9 374.187 662.536 256.065L656.842 259.141ZM581.945 107.518C499.236 18.8371 373.997 -18.4724 256.228 10.5134L259.436 16.4515C373.888 -10.991 495.248 25.1518 576.04 110.708L581.945 107.518Z" fill="#940c69"></path>
                            </svg>
                            <a href="#"><h4>Pet Clinic</h4></a>
                            <p> Merupakan klinik hewan yang didukung oleh Tenaga Medis Profesional</p>
                            </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="dogs-img">
                    <img src="{{ asset('user/img/dogs.png') }}" alt="dogs">
                </div>
            </div>
        </div>
    </div>
</section>
<section class="gap" style="background-image:url({{ asset('user/img/client-b.jpg') }})">
    <div class="container">
        <div class="heading">
            <img src="{{ asset('user/img/heading-img.png') }}" alt="heading-img">
            <h6>pelayanan</h6>
            <h2>Langkah Cepat</h2>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="works">
                    <div class="works-img">
                        <svg width="230" height="230" viewBox="0 0 673 673" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M9.82698 416.603C-19.0352 298.701 18.5108 173.372 107.497 90.7633L110.607 96.5197C24.3117 177.199 -12.311 298.935 15.0502 413.781L9.82698 416.603ZM89.893 565.433C172.674 654.828 298.511 692.463 416.766 663.224L414.077 658.245C298.613 686.363 175.954 649.666 94.9055 562.725L89.893 565.433ZM656.842 259.141C685.039 374.21 648.825 496.492 562.625 577.656L565.413 582.817C654.501 499.935 691.9 374.187 662.536 256.065L656.842 259.141ZM581.945 107.518C499.236 18.8371 373.997 -18.4724 256.228 10.5134L259.436 16.4515C373.888 -10.991 495.248 25.1518 576.04 110.708L581.945 107.518Z" fill="#000"></path>
                                </svg>
                        <i><img src="{{ asset('user/img/works-1.png') }}" alt="works"></i>
                        <span>1</span>
                    </div>
                    <h4>Cari Produk</h4>
                    <p>Telusuri katalog produk kami yang beragam untuk memenuhi kebutuhan hewan peliharaan Anda.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="works">
                    <div class="works-img">
                        <svg width="230" height="230" viewBox="0 0 673 673" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M9.82698 416.603C-19.0352 298.701 18.5108 173.372 107.497 90.7633L110.607 96.5197C24.3117 177.199 -12.311 298.935 15.0502 413.781L9.82698 416.603ZM89.893 565.433C172.674 654.828 298.511 692.463 416.766 663.224L414.077 658.245C298.613 686.363 175.954 649.666 94.9055 562.725L89.893 565.433ZM656.842 259.141C685.039 374.21 648.825 496.492 562.625 577.656L565.413 582.817C654.501 499.935 691.9 374.187 662.536 256.065L656.842 259.141ZM581.945 107.518C499.236 18.8371 373.997 -18.4724 256.228 10.5134L259.436 16.4515C373.888 -10.991 495.248 25.1518 576.04 110.708L581.945 107.518Z" fill="#000"></path>
                                </svg>
                        <i><img src="{{ asset('user/img/works-2.png') }}" alt="works"></i>
                        <span>2</span>
                    </div>
                    <h4>Pesan dan Bayar</h4>
                    <p>Beli dengan mudah, bayar dengan nyaman. Nikmati pengalaman pembayaran yang praktis dan aman</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="works mb-0">
                    <div class="works-img">
                        <svg width="230" height="230" viewBox="0 0 673 673" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M9.82698 416.603C-19.0352 298.701 18.5108 173.372 107.497 90.7633L110.607 96.5197C24.3117 177.199 -12.311 298.935 15.0502 413.781L9.82698 416.603ZM89.893 565.433C172.674 654.828 298.511 692.463 416.766 663.224L414.077 658.245C298.613 686.363 175.954 649.666 94.9055 562.725L89.893 565.433ZM656.842 259.141C685.039 374.21 648.825 496.492 562.625 577.656L565.413 582.817C654.501 499.935 691.9 374.187 662.536 256.065L656.842 259.141ZM581.945 107.518C499.236 18.8371 373.997 -18.4724 256.228 10.5134L259.436 16.4515C373.888 -10.991 495.248 25.1518 576.04 110.708L581.945 107.518Z" fill="#000"></path>
                                </svg>
                        <i><img src="{{ asset('user/img/works-3.png') }}" alt="works"></i>
                        <span>3</span>
                    </div>
                    <h4>Tunggu dengan Santai</h4>
                    <p>Relaksasi sambil menunggu. Nikmati waktu Anda tanpa stres menunggu produk sampai di depan pintu Anda</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- search-popup -->
<div class="search-popup">
    <button class="close-search style-two"><span class="flaticon-multiply"><i class="far fa-times-circle"></i></span></button>
    <button class="close-search"><i class="fa-solid fa-arrow-right"></i></button>
    <form method="get" action="{{ url('/shop') }}">
        <div class="form-group">
            <input type="search" name="search" value="" placeholder="Search Here" required="">
            <button type="submit"><i class="fa fa-search"></i></button>
        </div>
    </form>
</div>
<!-- search-popup end -->
@endsection


@section('js-extra')
    <script>
        $('.kategori-list').on('click', function() {
            var kategori_id = $(this).data('kategori_id');
            $('#input-kategori').val(kategori_id);
            $('#form-search')[0].submit();
        });

        $('#clear-search').on('click', function() {
            $('#input-search').val('');
            $('#input-kategori').val(0);
            $('#form-search')[0].submit();
        });

        $('.btn-add-to-cart').on('click', function(e) {
            if ($('#user_login').val() == 0) {
                window.location.href = "{{ url('/login-register') }}"
            } else {
                e.preventDefault();
                var id = $(this).data("id");
                var token = $(this).data("token");
    
                $.ajax({
                    type: "post",
                    url: '/cart',
                    data: {
                        "id": id,
                        "_method": 'POST',
                        "_token": token,
                    },
                    success: function (response) {
                        var title = 'Berhasil!';
                        var icon = 'success';
                        if (response.success) {
                            var title = 'Berhasil!';
                            var icon = 'success';
                            userCart();
                        } else {
                            var title = 'Perhatian!';
                            var icon = 'warning';
                        }
                        Swal.fire(
                            title,
                            response.message,
                            icon
                        );
                    }
                });
            }
        });
    </script>
@endsection