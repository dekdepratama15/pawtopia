@extends('layouts.user.app')

@section('content')

<section class="banner" style="background-color: #fff8e5; background-image:url({{asset('user/img/background-3.jpg')}})">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="banner-text">
                    <h2 class="text-white">User Dashboard</h2>
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item">
                        <a href="{{url('/')}}">Home</a>
                      </li>
                        <li class="breadcrumb-item active" aria-current="page">User Dashboard</li>
                    </ol>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="banner-img">
                    <div class="banner-img-1">
                        <svg width="260" height="260" viewBox="0 0 673 673" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M9.82698 416.603C-19.0352 298.701 18.5108 173.372 107.497 90.7633L110.607 96.5197C24.3117 177.199 -12.311 298.935 15.0502 413.781L9.82698 416.603ZM89.893 565.433C172.674 654.828 298.511 692.463 416.766 663.224L414.077 658.245C298.613 686.363 175.954 649.666 94.9055 562.725L89.893 565.433ZM656.842 259.141C685.039 374.21 648.825 496.492 562.625 577.656L565.413 582.817C654.501 499.935 691.9 374.187 662.536 256.065L656.842 259.141ZM581.945 107.518C499.236 18.8371 373.997 -18.4724 256.228 10.5134L259.436 16.4515C373.888 -10.991 495.248 25.1518 576.04 110.708L581.945 107.518Z" fill="#fa441d"></path>
                        </svg>
                        <img src="{{ asset('user/img/banner-img-1.jpg') }}" alt="banner">
                    </div>
                    <div class="banner-img-2">
                        <svg width="320" height="320" viewBox="0 0 673 673" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M9.82698 416.603C-19.0352 298.701 18.5108 173.372 107.497 90.7633L110.607 96.5197C24.3117 177.199 -12.311 298.935 15.0502 413.781L9.82698 416.603ZM89.893 565.433C172.674 654.828 298.511 692.463 416.766 663.224L414.077 658.245C298.613 686.363 175.954 649.666 94.9055 562.725L89.893 565.433ZM656.842 259.141C685.039 374.21 648.825 496.492 562.625 577.656L565.413 582.817C654.501 499.935 691.9 374.187 662.536 256.065L656.842 259.141ZM581.945 107.518C499.236 18.8371 373.997 -18.4724 256.228 10.5134L259.436 16.4515C373.888 -10.991 495.248 25.1518 576.04 110.708L581.945 107.518Z" fill="#fa441d"></path>
                        </svg>
                        <img src="{{ asset('user/img/banner-img-2.jpg') }}" alt="banner">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <img src="{{ asset('user/img/hero-shaps-1.png') }}" alt="hero-shaps" class="img-2">
    <img src="{{ asset('user/img/hero-shaps-1.png') }}" alt="hero-shaps" class="img-4">
</section>
<!-- my account wrapper start -->
<div class="my-account-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 mt-5 mb-5">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">Order</button>
                    <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="false">Account Detail</button>
                    <a class="nav-link" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();" data-bs-toggle="tab" data-bs-target="#nav-contact" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">Logout</a>
                </div>
            </nav>
                <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="myaccount-content mt-5">
                    <h3>Order</h3>
                    <div class="myaccount-table table-responsive text-center">
                        <table class="table table-bordered">
                            <thead class="thead-light">
                                <tr>
                                    <th>Order</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Total</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php 
                                    $status = [
                                        'pending' => 'Menunggu persetujuan Admin',
                                        'approve' => 'Disetujui oleh Admin',
                                        'payment_send' => 'Bukti pembayaran dikirim',
                                        'payment_approve' => 'Pembayaran diterima',
                                        'shipped' => 'Dikirim',
                                        'received' => 'Diterima',
                                        'done' => 'Selesai',
                                    ];
                                @endphp
                                @if(count($user->transaksis()->get()) > 0)
                                    @foreach($user->transaksis as $transaksi)
                                        <tr>
                                            <td>{{$transaksi->id}}{{count($transaksi->transaksi_details)}}{{date('Y', strtotime($transaksi->created_at))}}</td>
                                            <td>{{date('d M Y', strtotime($transaksi->created_at))}}</td>
                                            <td>{{$status[$transaksi->status]}}</td>
                                            <td>Rp. {{number_format($transaksi->total, 0, '', '.')}}</td>
                                            <td><a href="/user/transaksi/{{$transaksi->id}}" class="btn btn-sm btn-info">View</a></td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5">Tidak ada data transaksi</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <form class="checkout-meta donate-page mt-5 mb-5" action="/user/user/{{$user->id}}" method="post">
                        @method('PUT')
                        @csrf
                        <div class="row">
                            <div class="col-lg-8">
                                <h3>Account detail</h3>
                                <div class="col-lg-12">
                                    <label for="first-name" class="required">Username</label>
                                    <input type="text" id="first-name" name="name" value="{{$user->name}}" required />
                                    </select>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label for="first-name" class="required">Telp</label>
                                            <input type="text" id="first-telp" name="telp" value="{{$user->telp}}" required />
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="last-name" class="required">Email</label>
                                            <input type="email" id="email" name="email" value="{{$user->email}}" required readonly style="background-color: #f9f9f9" />
                                        </div>
                                    </div>
                                    <label for="display-name" class="required">Alamat</label>
                                    <input type="text" id="display-name" name="alamat" value="{{$user->alamat}}" required />
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label for="new-pwd" class="required">New Password</label>
                                            <input type="password" id="new-pwd" name="new_password" />
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="confirm-pwd" class="required">Confirm Password</label>
                                            <input type="password" id="confirm-pwd" name="confirm_password" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="button">Simpan</button>
                    </form>
            </div>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form><!-- My Account Page End -->
            </div>
        </div>
    </div>
</div>
@endsection