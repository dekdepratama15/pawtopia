@extends('layouts.user.app')

@section('content')
<section class="banner" style="background-color: #fff8e5; background-image:url({{asset('user/img/background-3.jpg')}})">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="banner-text">
                    <h2 class="text-white">Checkout</h2>
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item">
                        <a href="{{url('/')}}">Home</a>
                      </li>
                        <li class="breadcrumb-item active" aria-current="page">Checkout</li>
                    </ol>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="banner-img">
                    <div class="banner-img-1">
                        <svg width="260" height="260" viewBox="0 0 673 673" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M9.82698 416.603C-19.0352 298.701 18.5108 173.372 107.497 90.7633L110.607 96.5197C24.3117 177.199 -12.311 298.935 15.0502 413.781L9.82698 416.603ZM89.893 565.433C172.674 654.828 298.511 692.463 416.766 663.224L414.077 658.245C298.613 686.363 175.954 649.666 94.9055 562.725L89.893 565.433ZM656.842 259.141C685.039 374.21 648.825 496.492 562.625 577.656L565.413 582.817C654.501 499.935 691.9 374.187 662.536 256.065L656.842 259.141ZM581.945 107.518C499.236 18.8371 373.997 -18.4724 256.228 10.5134L259.436 16.4515C373.888 -10.991 495.248 25.1518 576.04 110.708L581.945 107.518Z" fill="#fa441d"></path>
                        </svg>
                        <img src="{{ asset('user/img/banner-img-1.jpg') }}" alt="banner">
                    </div>
                    <div class="banner-img-2">
                        <svg width="320" height="320" viewBox="0 0 673 673" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M9.82698 416.603C-19.0352 298.701 18.5108 173.372 107.497 90.7633L110.607 96.5197C24.3117 177.199 -12.311 298.935 15.0502 413.781L9.82698 416.603ZM89.893 565.433C172.674 654.828 298.511 692.463 416.766 663.224L414.077 658.245C298.613 686.363 175.954 649.666 94.9055 562.725L89.893 565.433ZM656.842 259.141C685.039 374.21 648.825 496.492 562.625 577.656L565.413 582.817C654.501 499.935 691.9 374.187 662.536 256.065L656.842 259.141ZM581.945 107.518C499.236 18.8371 373.997 -18.4724 256.228 10.5134L259.436 16.4515C373.888 -10.991 495.248 25.1518 576.04 110.708L581.945 107.518Z" fill="#fa441d"></path>
                        </svg>
                        <img src="{{ asset('user/img/banner-img-2.jpg') }}" alt="banner">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <img src="{{ asset('user/img/hero-shaps-1.png') }}" alt="hero-shaps" class="img-2">
    <img src="{{ asset('user/img/hero-shaps-1.png') }}" alt="hero-shaps" class="img-4">
</section>

<section class="gap">
    <div class="container">
        <div class="row">
            <div style="overflow-x:auto;overflow-y: hidden;">
              <table class="shop_table table-responsive">
                    <thead>
                        <tr>
                            <th class="product-name">Product</th>
                            <th class="product-price">Price</th>
                            <th class="product-quantity">Quantity</th>
                            <th class="product-subtotal">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $total = 0;
                        @endphp
                        @foreach($user->carts as $cart)
                            @php
                                $total += ($cart->produk->harga * $cart->qty);
                            @endphp
                            <tr>
                                <td class="product-name">
                                    <img alt="img" src="{{url('upload/'.$cart->produk->gambar)}}" style="width: 222px; height: 278px; object-fit: cover !important">
                                    <div>
                                        <a href="{{url('/produk-detail/'.$cart->produk->id)}}">{{$cart->produk->nama_produk}}</a>
                                    </div>
                                </td>
                                <td class="product-price">
                                    <span class="woocommerce-Price-amount"><bdi><span class="woocommerce-Price-currencySymbol">Rp. </span>{{ number_format($cart->produk->harga, 0, '.', ',') }}</bdi>
                                    </span>          
                                </td>
                                <td class="product-quantity">
                                    {{$cart->qty}}
                                </td>
                                <td class="product-subtotal">
                                    <span class="woocommerce-Price-amount"><bdi><span class="woocommerce-Price-currencySymbol">Rp. </span>{{ number_format(($cart->produk->harga * $cart->qty), 0, '.', ',') }}</bdi></span>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
              </table>
            </div>
                <div class="row mt-lg-5">
                    <div class="col-lg-12">
                        <div class="cart_totals">
                            <h4>Payment Total</h4>
                            <table class="shop_table_responsive">
                                <tbody>
                                    <tr class="cart-subtotal">
                                        <th>Subtotal:</th>
                                        <td>
                                            <span class="woocommerce-Price-amount">
                                            <bdi>
                                                <span class="woocommerce-Price-currencySymbol">Rp. </span>{{ number_format($total, 0, '.', ',') }}
                                            </bdi>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="Shipping">
                                        <th>Shipping:</th>
                                        <td>
                                            <span class="woocommerce-Price-amount amount">
                                                free
                                            </span>
                                         </td>
                                    </tr>
                                    <tr class="Total">
                                        <th>Total:</th>
                                        <td>
                                            <span class="woocommerce-Price-amount">
                                            <bdi>
                                                <span>Rp. </span> {{ number_format($total, 0, '.', ',') }}
                                            </bdi>
                                            </span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="wc-proceed-to-checkout">
                            <form action="/user/checkout/save" method="post">
                                @csrf
                                <input type="hidden" name="total" value="{{$total}}">
                                <button type="submit" class="button">Proses Checkout</button>
                            </form>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</section>
@endsection