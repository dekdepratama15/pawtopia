@extends('layouts.user.app')

@section('content')
<section class="banner" style="background-color: #fff8e5; background-image:url({{asset('user/img/background-3.jpg')}})">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="banner-text">
                    <h2 class="text-white">product details</h2>
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item">
                        <a href="{{url('/')}}">Home</a>
                      </li>
                        <li class="breadcrumb-item active" aria-current="page">product details</li>
                    </ol>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="banner-img">
                    <div class="banner-img-1">
                        <svg width="260" height="260" viewBox="0 0 673 673" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M9.82698 416.603C-19.0352 298.701 18.5108 173.372 107.497 90.7633L110.607 96.5197C24.3117 177.199 -12.311 298.935 15.0502 413.781L9.82698 416.603ZM89.893 565.433C172.674 654.828 298.511 692.463 416.766 663.224L414.077 658.245C298.613 686.363 175.954 649.666 94.9055 562.725L89.893 565.433ZM656.842 259.141C685.039 374.21 648.825 496.492 562.625 577.656L565.413 582.817C654.501 499.935 691.9 374.187 662.536 256.065L656.842 259.141ZM581.945 107.518C499.236 18.8371 373.997 -18.4724 256.228 10.5134L259.436 16.4515C373.888 -10.991 495.248 25.1518 576.04 110.708L581.945 107.518Z" fill="#fa441d"></path>
                        </svg>
                        <img src="{{ asset('user/img/banner-img-1.jpg') }}" alt="banner">
                    </div>
                    <div class="banner-img-2">
                        <svg width="320" height="320" viewBox="0 0 673 673" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M9.82698 416.603C-19.0352 298.701 18.5108 173.372 107.497 90.7633L110.607 96.5197C24.3117 177.199 -12.311 298.935 15.0502 413.781L9.82698 416.603ZM89.893 565.433C172.674 654.828 298.511 692.463 416.766 663.224L414.077 658.245C298.613 686.363 175.954 649.666 94.9055 562.725L89.893 565.433ZM656.842 259.141C685.039 374.21 648.825 496.492 562.625 577.656L565.413 582.817C654.501 499.935 691.9 374.187 662.536 256.065L656.842 259.141ZM581.945 107.518C499.236 18.8371 373.997 -18.4724 256.228 10.5134L259.436 16.4515C373.888 -10.991 495.248 25.1518 576.04 110.708L581.945 107.518Z" fill="#fa441d"></path>
                        </svg>
                        <img src="{{ asset('user/img/banner-img-2.jpg') }}" alt="banner">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <img src="{{ asset('user/img/hero-shaps-1.png') }}" alt="hero-shaps" class="img-2">
    <img src="{{ asset('user/img/hero-shaps-1.png') }}" alt="hero-shaps" class="img-4">
</section>
<section class="gap no-bottom">
  <div class="container">
    <div class="row product-info-section">
      <div class="col-lg-7 p-0">
        <div class="pd-gallery">
            <div class="pd-main-img">
              <img id="NZoomImg" alt="toys" src="{{url('upload/'.$produk->gambar)}}" style="width: 416px; height: 367px; object-fit: cover !important"  >
              
            </div>
        </div>
      </div>
      <div class="col-lg-5">
        <div class="product-info p-60">
          <div class="d-flex align-items-center">
            <div class="start d-flex align-items-center">
                @php 
                    $rating = !empty($produk->rating) ? $produk->rating : 0;
                @endphp
                @for($i = 1; $i <= 5; $i++)
                    @if($rating - $i >= 0)
                        <li><i class="fa-solid fa-star"></i></li>
                    @elseif($rating - $i < 0 && $rating - $i > -1)
                        <li><i class="fa-solid fa-star-half-alt"></i></li>
                    @else
                        <li><i class="fa-regular fa-star"></i></li>
                    @endif
                @endfor
            </div>
          </div>
          <h3>{{$produk->nama_produk}}</h3>
          <form class="variations_form">
            <div class="stock">
            <span class="price">
              <ins>
                  <span class="woocommerce-Price-amount amount">
                    <bdi>
                    <span class="woocommerce-Price-currencySymbol">Rp</span>{{ number_format($produk->harga, 0, '.', ',') }}</bdi>
                  </span>
                </ins> 
            </span>
            </div>
                <div class="add-to-cart">
                          <a type="button" class="button btn-add-to-cart" data-id="{{$produk->id}}" data-token="{{ csrf_token() }}">Add to Cart</a>
                        </div>
            <ul class="product_meta">
                <li><span class="theme-bg-clr">Stok:</span>
                  <ul class="pd-cat">
                    <li><a href="#">{{$produk->stok}}</a></li>          
                  </ul>
                </li>
                <li><span class="theme-bg-clr">Category:</span>
                  <ul class="pd-tag">
                     <li>
                       <a href="#">{{$produk->kategori->nama_kategori}}</a>
                     </li>    
                  </ul>
                </li>
              </ul>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="gap">
  <div class="container">
    <div class="information">
      <h3>Deskripsi</h3>
      <div class="boder-bar"></div>
      <p>Lorem ipsum dolor sit amet,consectetur adipiscing elit do eiusmod tempor incididunt ut labore et.Lorem ipsumsit amet, consectetur adipiscing elit, sed do eiusmod teincididunt ut la amet,consectetur adipiscing elibore et. Lorem ipsum dolor sit amet,consectetur adipiscing elit do eiusmod tempor incididunt ut labore et.Lorem ipsumsit amet, consectetur adipiscing elit, sed do eiusmod teincididunt ut la amet,consectetur adipiscing elibore et. loLorem ipsum dolor sit amet,consectetur adipiscing elit do eiusmod tempor incididunt ut labore et.Lorem ipsumsit amet, consectetur adipiscing elit, sed do eiusmod teincididunt ut la amet,consectetur adipiscing elibore et. Lorem ipsum dolor sit amet,consectetur adipiscing elit do eiusmod tempor incididunt ut labore et.Lorem ipsumsit amet, consectetur adipiscing elit, </p>
    </div>
  </div>
</section>

@if(count($related) > 0)
    <section class="gap no-top products-section">
    <div class="container">
        <div class="information">
        <h3>Related Produk</h3>
        <div class="boder-bar"></div>
        </div>
        <div class="row">
            @foreach($related as $relate)
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="healthy-product mb-lg-0">
                    <div class="healthy-product-img">
                        <img src="{{ url('upload/'.$relate->gambar) }}" style="width: 217px; height: 255px; object-fit: cover !important" alt="product">
                        <ul class="star">
                            @php 
                                $rating = !empty($relate->rating) ? $relate->rating : 0;
                            @endphp
                            @for($i = 1; $i <= 5; $i++)
                                @if($rating - $i >= 0)
                                    <li><i class="fa-solid fa-star"></i></li>
                                @elseif($rating - $i < 0 && $rating - $i > -1)
                                    <li><i class="fa-solid fa-star-half-alt"></i></li>
                                @else
                                    <li><i class="fa-regular fa-star"></i></li>
                                @endif
                            @endfor
                        </ul>
                        <div class="add-to-cart">
                            <a type="button" class="btn-add-to-cart" data-id="{{$relate->id}}" data-token="{{ csrf_token() }}">Add to Cart</a>
                        </div>
                    </div>
                    <span>{{$relate->kategori->nama_kategori}}</span>
                    <a href="{{url('/produk-detail/'.$relate->id)}}">{{ $relate->nama_produk }}</a>
                    <h6>Rp. {{ number_format($relate->harga, 0, '.', ',') }}</h6>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    </section>
@endif
@endsection

@section('js-extra')
    <script>
        $('.kategori-list').on('click', function() {
            var kategori_id = $(this).data('kategori_id');
            $('#input-kategori').val(kategori_id);
            $('#form-search')[0].submit();
        });

        $('#clear-search').on('click', function() {
            $('#input-search').val('');
            $('#input-kategori').val(0);
            $('#form-search')[0].submit();
        });

        $('.btn-add-to-cart').on('click', function(e) {
            if ($('#user_login').val() == 0) {
                window.location.href = "{{ url('/login-register') }}"
            } else {
                e.preventDefault();
                var id = $(this).data("id");
                var token = $(this).data("token");
    
                $.ajax({
                    type: "post",
                    url: '/cart',
                    data: {
                        "id": id,
                        "_method": 'POST',
                        "_token": token,
                    },
                    success: function (response) {
                        var title = 'Berhasil!';
                        var icon = 'success';
                        if (response.success) {
                            var title = 'Berhasil!';
                            var icon = 'success';
                            userCart();
                        } else {
                            var title = 'Perhatian!';
                            var icon = 'warning';
                        }
                        Swal.fire(
                            title,
                            response.message,
                            icon
                        );
                    }
                });
            }
        });
    </script>
@endsection