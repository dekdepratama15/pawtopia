@extends('layouts.user.app')

@section('content')
<section class="banner" style="background-color: #fff8e5; background-image:url({{asset('user/img/background-3.jpg')}})">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="banner-text">
                    <h2 class="text-white">Shop</h2>
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item">
                        <a href="{{ url('/') }}">Home</a>
                      </li>
                        <li class="breadcrumb-item active" aria-current="page">Shop</li>
                    </ol>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="banner-img">
                    <div class="banner-img-1">
                        <svg width="260" height="260" viewBox="0 0 673 673" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M9.82698 416.603C-19.0352 298.701 18.5108 173.372 107.497 90.7633L110.607 96.5197C24.3117 177.199 -12.311 298.935 15.0502 413.781L9.82698 416.603ZM89.893 565.433C172.674 654.828 298.511 692.463 416.766 663.224L414.077 658.245C298.613 686.363 175.954 649.666 94.9055 562.725L89.893 565.433ZM656.842 259.141C685.039 374.21 648.825 496.492 562.625 577.656L565.413 582.817C654.501 499.935 691.9 374.187 662.536 256.065L656.842 259.141ZM581.945 107.518C499.236 18.8371 373.997 -18.4724 256.228 10.5134L259.436 16.4515C373.888 -10.991 495.248 25.1518 576.04 110.708L581.945 107.518Z" fill="#fa441d"></path>
                        </svg>
                        <img src="{{ asset('user/img/banner-img-1.jpg') }}" alt="banner">
                    </div>
                    <div class="banner-img-2">
                        <svg width="320" height="320" viewBox="0 0 673 673" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M9.82698 416.603C-19.0352 298.701 18.5108 173.372 107.497 90.7633L110.607 96.5197C24.3117 177.199 -12.311 298.935 15.0502 413.781L9.82698 416.603ZM89.893 565.433C172.674 654.828 298.511 692.463 416.766 663.224L414.077 658.245C298.613 686.363 175.954 649.666 94.9055 562.725L89.893 565.433ZM656.842 259.141C685.039 374.21 648.825 496.492 562.625 577.656L565.413 582.817C654.501 499.935 691.9 374.187 662.536 256.065L656.842 259.141ZM581.945 107.518C499.236 18.8371 373.997 -18.4724 256.228 10.5134L259.436 16.4515C373.888 -10.991 495.248 25.1518 576.04 110.708L581.945 107.518Z" fill="#fa441d"></path>
                        </svg>
                        <img src="{{ asset('user/img/banner-img-2.jpg') }}" alt="banner">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <img src="{{ asset('user/img/hero-shaps-1.png') }}" alt="hero-shaps" class="img-2">
    <img src="{{ asset('user/img/hero-shaps-1.png') }}" alt="hero-shaps" class="img-4">
</section>
<section class="gap products-section">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="items-number">
          <span id="search">{{ isset($form['search']) && !empty($form['search']) ? 'Search : '.$form['search'] : 'All Item' }}</span>
          <input type="hidden" id="search_hidden" value="{{ isset($form['search']) ? $form['search'] : '' }}">
          <div class="d-flex align-items-center">
            <span>Category</span>
            <select class="nice-select Advice" id="kategori_select">
                <option>Semua</option>
                @foreach($kategoris as $kategori)
                    <option value="{{$kategori->id}}" {{isset($form['kategori']) && $form['kategori'] == $kategori->id ? 'selected' : ''}}>{{$kategori->nama_kategori}}</option>
                @endforeach
            </select>
          </div>
        </div>
        <div class="row">
            @foreach($produks as $produk)
            <div class="col-md-4 col-sm-6">
                <div class="healthy-product">
                    <div class="healthy-product-img">
                        <img src="{{ url('upload/'.$produk->gambar) }}" style="width: 222px; height: 278px; object-fit: cover !important" alt="product">
                        <ul class="star">
                            @php 
                                $rating = !empty($produk->rating) ? $produk->rating : 0;
                            @endphp
                            @for($i = 1; $i <= 5; $i++)
                                @if($rating - $i >= 0)
                                    <li><i class="fa-solid fa-star"></i></li>
                                @elseif($rating - $i < 0 && $rating - $i > -1)
                                    <li><i class="fa-solid fa-star-half-alt"></i></li>
                                @else
                                    <li><i class="fa-regular fa-star"></i></li>
                                @endif
                            @endfor
                        </ul>
                        <div class="add-to-cart">
                          <a type="button" class="btn-add-to-cart" data-id="{{$produk->id}}" data-token="{{ csrf_token() }}">Add to Cart</a>
                        </div>
                    </div>
                    <span>{{$produk->kategori->nama_kategori}}</span>
                    <a href="{{url('/produk-detail/'.$produk->id)}}">{{ $produk->nama_produk }}</a>
                    <h6>Rp. {{ number_format($produk->harga, 0, '.', ',') }}</h6>
                </div>
            </div>
            @endforeach
        </div>
        <div class="row d-flex justify-content-center">
            {{$produks->appends($form)->links()}}
        </div>
      </div>
    </div>
  </div>
</section>
<!-- search-popup -->
<div class="search-popup">
    <button class="close-search style-two"><span class="flaticon-multiply"><i class="far fa-times-circle"></i></span></button>
    <button class="close-search"><i class="fa-solid fa-arrow-right"></i></button>
    <form method="get" action="{{ url('/shop') }}">
        <div class="form-group">
            <input type="hidden" name="kategori" value="{{ isset($form['kategori']) ? $form['kategori'] : 0 }}">
            <input type="search" name="search" value="{{ isset($form['search']) ? $form['search'] : '' }}" placeholder="Search Here">
            <button type="submit"><i class="fa fa-search"></i></button>
        </div>
    </form>
</div>
<!-- search-popup end -->
@endsection

@section('js-extra')
    <script>
        $(document).ready(function () {
            $('#kategori_select').on('change', function(e) {
                window.location.href = "{{ url('/shop') }}?search=" + $('#search_hidden').val() + '&kategori='+ e.target.value
            });
        });

        $('.btn-add-to-cart').on('click', function(e) {
            if ($('#user_login').val() == 0) {
                window.location.href = "{{ url('/login-register') }}"
            } else {
                e.preventDefault();
                var id = $(this).data("id");
                var token = $(this).data("token");
    
                $.ajax({
                    type: "post",
                    url: '/cart',
                    data: {
                        "id": id,
                        "_method": 'POST',
                        "_token": token,
                    },
                    success: function (response) {
                        var title = 'Berhasil!';
                        var icon = 'success';
                        if (response.success) {
                            var title = 'Berhasil!';
                            var icon = 'success';
                            userCart();
                        } else {
                            var title = 'Perhatian!';
                            var icon = 'warning';
                        }
                        Swal.fire(
                            title,
                            response.message,
                            icon
                        );
                    }
                });
            }
        });
    </script>
@endsection