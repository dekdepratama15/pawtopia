@extends('layouts.admin.app')

@section('content')

<main id="main-container">
    <!-- Hero -->
    <div class="bg-body-light">
      <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
          <h1 class="flex-grow-1 fs-3 fw-semibold my-2 my-sm-3">User</h1>
        </div>
      </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
      <!-- Dynamic Table Full -->
      <div class="block block-rounded">
        <div class="block-header row pt-4">
            <div class="col-lg-6">
                <h3 class="block-title">
                    Data <b>User</b>
                </h3>
            </div>
            {{-- <div class="col-lg-6 text-end">
                <a type="button" href="{{url('/admin/produk/create')}}" class="btn btn-sm btn-dark" >Tambah Produk</a>
            </div> --}}
        </div>
        <div class="block-content block-content-full">
          <table class="table table-bordered table-striped table-vcenter data-table">
            <thead>
              <tr>
                <th>Nama</th>
                <th>Email</th>
                <th>Role</th>
                <th>No Telp</th>
                <th>Alamat</th>
                <th width=30% >Action</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($user as $item)
            <tr>
                <td>{{$item->name}}</td>
                <td>{{$item->email}}</td>
                <td>{{$item->role}}</td>
                <td>{{$item->telp}}</td>
                <td>{{$item->alamat}}</td>
                {{-- <td class="text-center align-items-center" ><img style="max-width: 150px;" src="{{url('upload/'.$item->gambar)}}" alt=""></td> --}}
                <td>
                    <button type="button" class="btn btn-sm btn-danger delete-data" data-redirect="user" data-id="{{ $item->id }}" data-token="{{ csrf_token() }}">Delete</button>
                </td>
            </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <!-- END Dynamic Table Full -->
    </div>
    <!-- END Page Content -->
  </main>

@endsection