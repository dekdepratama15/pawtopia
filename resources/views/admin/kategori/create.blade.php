@extends('layouts.admin.app')

@section('content')

<main id="main-container">
    <!-- Hero -->
    <div class="bg-body-light">
      <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
          <h1 class="flex-grow-1 fs-3 fw-semibold my-2 my-sm-3">Tambah Kategori</h1>
        </div>
      </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
      <!-- Dynamic Table Full -->
      <div class="block block-rounded">
        <div class="block-content block-content-full">
          <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _js/pages/be_tables_datatables.js -->
          <form action="/admin/kategori" method="post">
            @csrf
            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label mb-1" for="namaKategori">Nama Kategori</label>
                                        <input type="text" class="form-control mt-1" id="namaKategori" placeholder="Masukkan Nama Kategori" name="nama_kategori" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="box">
                        <div class="box-body">
                            <div class="col-lg-12 text-end">
                                <a type="button" href="{{ url('/kategori') }}" class="btn btn-secondary">Kembali</a>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
          </form>
        </div>
      </div>
      <!-- END Dynamic Table Full -->
      <!-- Dynamic Table Responsive -->
    </div>
    <!-- END Page Content -->
  </main>

@endsection