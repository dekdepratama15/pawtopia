@extends('layouts.admin.app')

@section('content')

<main id="main-container">
    <!-- Hero -->
    <div class="bg-body-light">
      <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
          <h1 class="flex-grow-1 fs-3 fw-semibold my-2 my-sm-3">Kategori</h1>
        </div>
      </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
      <!-- Dynamic Table Full -->
      <div class="block block-rounded">
        <div class="block-header row pt-4">
            <div class="col-lg-6">
                <h3 class="block-title">
                    Data <b>Kategori</b>
                </h3>
            </div>
            <div class="col-lg-6 text-end">
                <a type="button" href="{{url('/admin/kategori/create')}}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Kategori</a>
            </div>
        </div>
        <div class="block-content block-content-full">
          <table class="table table-bordered table-striped table-vcenter data-table">
            <thead>
              <tr>
                <th>Nama Kategori</th>
                <th width=30% >Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($kategori as $k)
                <tr>
                    <td>{{$k->nama_kategori}}</td>
                    <td>
                        <a type="button" href="{{route('kategori.edit', $k->id)}}" class="btn btn-info btn-sm"><i class="fa fa-pen"></i> Edit</a>
                        <button type="button" class="btn btn-sm btn-danger delete-data" data-redirect="kategori" data-id="{{ $k->id }}" data-token="{{ csrf_token() }}"><i class="fa fa-trash"></i> Delete</button>
                    </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <!-- END Dynamic Table Full -->
      <!-- Dynamic Table Responsive -->
    </div>
    <!-- END Page Content -->
  </main>

@endsection
