@extends('layouts.admin.app')

@section('content')

<main id="main-container">
    <!-- Hero -->
    <div class="bg-body-light">
      <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
          <h1 class="flex-grow-1 fs-3 fw-semibold my-2 my-sm-3">Edit Kategori</h1>
        </div>
      </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
      <!-- Dynamic Table Full -->
      <div class="block block-rounded">
        <div class="block-content block-content-full">
          <form action="{{ route('merk.update', $merk->id) }}" method="post" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label mb-1" for="namaMerk">Nama Merk</label>
                                        <input type="text" class="form-control mt-1" id="namaMerk" placeholder="Masukkan Nama Merk" name="nama_merk" value="{{$merk->nama_merk}}"  required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                      <label class="form-control-label mb-1" for="gambar">Gambar</label>
                                      <input type="file" class="form-control mt-1" id="gambar" placeholder="Masukkan Rating Produk" name="gambar">
                                      <a type="button" class="btn btn-sm btn-warning mt-3" href="{{url('upload/'.$merk->gambar)}}" data-fancybox="images" data-caption="Foto {{$merk->nama_merk}}">Lihat Gambar</a>
                                  </div>
                              </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="box">
                        <div class="box-body">
                            <div class="col-lg-12 text-end">
                                <a type="button" href="{{ url('/admin/merk') }}" class="btn btn-secondary">Kembali</a>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
          </form>
        </div>
      </div>
      <!-- END Dynamic Table Full -->
      <!-- Dynamic Table Responsive -->
    </div>
    <!-- END Page Content -->
  </main>

@endsection