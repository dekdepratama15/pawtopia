<!DOCTYPE html>
<html>
<head>
    <title>Cuti</title>
</head>
<body>
    @php 
        $status = [
            'pending' => 'Menunggu persetujuan Admin',
            'approve' => 'Disetujui oleh Admin',
            'payment_send' => 'Bukti pembayaran dikirim',
            'payment_approve' => 'Pembayaran diterima',
            'shipped' => 'Dikirim',
            'received' => 'Diterima',
            'done' => 'Selesai',
        ];
    @endphp
    <table>
        <thead>
            <tr>
                <th><b>#</b></th>
                <th><b>Nama Pembeli</b></th>
                <th><b>Tanggal Transaksi</b></th>
                <th><b>Total Harga</b></th>
                <th><b>Status</b></th>
            </tr>
        </thead>
        <tbody>
            @foreach($transaksis as $key => $transaksi)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$transaksi->user->name}}</td>
                    <td>{{date('Y-m-d', $transaksi->tgl_mulai)}}</td>
                    <td>{{$transaksi->total}}</td>
                    <td>{{$status[$transaksi->status]}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>