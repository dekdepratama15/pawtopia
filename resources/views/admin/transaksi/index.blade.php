@extends('layouts.admin.app')

@section('content')

<main id="main-container">
    <!-- Hero -->
    <div class="bg-body-light">
      <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
          <h1 class="flex-grow-1 fs-3 fw-semibold my-2 my-sm-3">Transaksi</h1>
        </div>
      </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
      <!-- Dynamic Table Full -->
      <div class="block block-rounded">
        <div class="block-header row pt-4">
            <div class="col-lg-6">
                <h3 class="block-title">
                    Data <b>Transaksi</b>
                </h3>
            </div>
        </div>
        <div class="block-content block-content-full">
          <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _js/pages/be_tables_datatables.js -->
          <table class="table table-bordered table-striped table-vcenter data-table">
            <thead>
              <tr>
                <th>No</th>
                <th>User</th>
                <th>Tanggal Transaksi</th>
                <th>Total Harga</th>
                <th>Status</th>
                <th>Bukti Pembayaran</th>
                <th width=20% >Action</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($transaksi as $item)
            @php 
                $status = [
                    'pending' => 'Menunggu persetujuan Admin',
                    'approve' => 'Disetujui oleh Admin',
                    'payment_send' => 'Bukti pembayaran dikirim',
                    'payment_approve' => 'Pembayaran diterima',
                    'shipped' => 'Dikirim',
                    'received' => 'Diterima',
                    'done' => 'Selesai',
                ];
            @endphp
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$item->user->name}}</td>
                <td>{{date('d M Y', strtotime($item->created_at))}}</td>
                <td>Rp. {{number_format($item->total, 0, '', '.')}} </td>
                <td>{{$status[$item->status]}}</td>
                <td class="text-center align-items-center" ><img style="max-width: 150px;" src="{{url('upload/'.$item->bukti_bayar)}}" alt=""></td>
                <td>
                    <a type="button" href="{{route('transaksi.show', $item->id)}}" class="btn btn-warning btn-sm" ><i class="fa fa-eye"></i> Detail</a>
                    @if ($item->status == 'pending' )
                        <button type="button" class="btn btn-sm btn-primary change-status-data" data-status="approve" data-id="{{ $item->id }}" data-token="{{ csrf_token() }}">Setujui</button>  
                    @endif
                    @if ($item->status == 'payment_send' )
                        <button type="button" class="btn btn-sm btn-dark change-status-data" data-status="payment_approve" data-id="{{ $item->id }}" data-token="{{ csrf_token() }}">Terima Pembayaran</button>  
                    @endif
                    @if ($item->status == 'payment_approve' )
                        <button type="button" class="btn btn-sm btn-info change-status-data" data-status="shipped" data-id="{{ $item->id }}" data-token="{{ csrf_token() }}">Kirim Barang</button>  
                    @endif
                </td>
            </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <!-- END Dynamic Table Full -->
    </div>
    <!-- END Page Content -->
  </main>

@endsection

@section('js-extra')
    <script>
        $(document).ready(function () {
            $('.change-status-data').click(function (e) { 
            e.preventDefault();
            console.log('asas');
            var id = $(this).data("id");
            var token = $(this).data("token");
            var status = $(this).data("status");

            Swal.fire({
                title: 'Yakin?',
                text: "Ingin Mengubah Status",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#0275d8',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ubah',
                cancelButtonText: 'Batal'
            })
            .then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "PUT",
                        url: '/admin/transaksi/'+id,
                        data: {
                            "id": id,
                            "status": status,
                            "_method": 'PUT',
                            "_token": token,
                        },
                        success: function (response) {
                            Swal.fire(
                                'Berhasil!',
                                response.message,
                                'success'
                            )
                            .then((result) => {
                                location.reload();
                            });

                        }
                    });
                }
            })
        });
        })
    </script>
@endsection