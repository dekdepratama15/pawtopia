@extends('layouts.admin.app')

@section('content')

<main id="main-container">
    <!-- Hero -->
    <div class="bg-body-light">
      <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
          <h1 class="flex-grow-1 fs-3 fw-semibold my-2 my-sm-3">Detail Transaksi</h1>
        </div>
      </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
      <!-- Dynamic Table Full -->
      <div class="block block-rounded">
        <div class="block-content block-content-full">
          <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _js/pages/be_tables_datatables.js -->
          <form action="/produk" method="post" enctype="multipart/form-data">
            @csrf
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-grow-1 fs-3 fw-semibold my-2 my-sm-3">Detail Transaksi</h1>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label mb-1" for="namaProduk">Nama User</label>
                                        <input type="text" class="form-control mt-1" id="namaProduk" placeholder="" name="nama_produk" value="{{$transaksi->user->name}}" readonly required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label mb-1" for="namaProduk">No Transaksi</label>
                                        <input type="text" class="form-control mt-1" id="namaProduk" placeholder="" name="nama_produk" value="{{$transaksi->id}}" readonly required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label mb-1" for="namaProduk">Tanggal Transaksi</label>
                                        <input type="text" class="form-control mt-1" id="namaProduk" placeholder="" name="nama_produk" value="{{date('d M Y', strtotime($transaksi->created_at))}}" readonly required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-content-full" >
                    <table class="table table-bordered table-striped table-vcenter js-dataTable-full" >
                        <thead>
                            <tr>
                                <th>Gambar Produk</th>
                                <th>Nama Produk</th>
                                <th>Jumlah</th>
                                <th>Harga</th>
                                <th>Total Harga</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($transaksi->transaksi_details as $item)
                                <tr>
                                    <td class="text-center align-items-center" ><img style="max-width: 150px;" src="{{url('upload/'.$item->produk->gambar)}}" alt=""></td>
                                    <td>{{$item->produk->nama_produk}}</td>
                                    <td>{{$item->qty}}</td>
                                    <td>Rp. {{number_format($item->harga_beli, 0, '', '.')}} </td>
                                    <td>Rp. {{number_format($item->harga_beli*$item->qty, 0, '', '.')}} </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-control-label mb-1" for="namaProduk">Total Harga</label>
                                    <input type="text" class="form-control mt-1" id="namaProduk" placeholder="" name="nama_produk" value="Rp. {{number_format($transaksi->total, 0, '', '.')}}" readonly required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-control-label mb-1" for="namaProduk">Bukti Pembayaran</label>
                                    <div>
                                        <img src="{{url('upload/'.$transaksi->bukti_bayar)}}" alt="" width="200" height="200">
                                    </div>
                                    <a type="button" class="btn btn-sm btn-warning mt-2" href="{{url('upload/'.$transaksi->bukti_bayar)}}" data-fancybox="images">Lihat Bukti Pembayaran</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
          </form>
        </div>
      </div>
      <div class="block block-rounded">
        <div class="block-content block-content-full">
          <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _js/pages/be_tables_datatables.js -->
          <form action="/produk" method="post" enctype="multipart/form-data">
            @csrf
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-grow-1 fs-3 fw-semibold my-2 my-sm-3">Detail Rating</h1>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-body">
                        </div>
                    </div>
                </div>
                <div class="block-content-full" >
                    <table class="table table-bordered table-striped table-vcenter js-dataTable-full" >
                        <thead>
                            <tr>
                                <th>Gambar Produk</th>
                                <th>Nama Produk</th>
                                <th>Rating</th>
                                <th>Deskripsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($transaksi->transaksi_details as $item)
                                <tr>
                                    <td class="text-center align-items-center" ><img style="max-width: 150px;" src="{{url('upload/'.$item->produk->gambar)}}" alt=""></td>
                                    <td>{{$item->produk->nama_produk}}</td>
                                    <td>
                                        @for($i=1;$i<=5;$i++)
                                            <i class="fa fa-star {{$i <= $item->rating ? 'text-warning' : ''}}"></i>
                                        @endfor
                                    </td>
                                    <td>{{$item->deskripsi_rating}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-12">
                    <div class="box">
                        <div class="box-body">
                            <div class="col-lg-12 text-end">
                                <a type="button" href="{{ url('/admin/transaksi') }}" class="btn btn-secondary">Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
          </form>
        </div>
      </div>
      <!-- END Dynamic Table Full -->
      <!-- Dynamic Table Responsive -->
    </div>
    <!-- END Page Content -->
  </main>

@endsection