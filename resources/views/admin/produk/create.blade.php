@extends('layouts.admin.app')

@section('content')

<main id="main-container">
    <!-- Hero -->
    <div class="bg-body-light">
      <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
          <h1 class="flex-grow-1 fs-3 fw-semibold my-2 my-sm-3">Tambah Produk</h1>
        </div>
      </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
      <!-- Dynamic Table Full -->
      <div class="block block-rounded">
        <div class="block-content block-content-full">
          <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _js/pages/be_tables_datatables.js -->
          <form action="/admin/produk" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label mb-1" for="namaProduk">Nama Produk</label>
                                        <input type="text" class="form-control mt-1" id="namaProduk" placeholder="Masukkan Nama Produk" name="nama_produk" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label mb-1" for="kategori">Kategori</label>
                                        <select name="kategori" id="kategori" class="form-control">
                                            <option value="">-Pilih Kategori-</option>
                                            @foreach ($kategori as $k)
                                                <option value="{{$k->id}}">{{$k->nama_kategori}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label mb-1" for="merk">Merk</label>
                                        <select name="merk" id="merk" class="form-control">
                                            <option value="">-Pilih Merk-</option>
                                            @foreach ($merk as $m)
                                                <option value="{{$m->id}}">{{$m->nama_merk}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label mb-1" for="hargaProduk">Harga</label>
                                        <input type="text" class="form-control mt-1" id="hargaProduk" placeholder="Masukkan Harga Produk" name="harga" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label mb-1" for="stokProduk">Stok</label>
                                        <input type="number" class="form-control mt-1" id="stokProduk" placeholder="Masukkan Stok Produk" name="stok" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label mb-1" for="deskripsiProduk">Deskripsi</label>
                                        <textarea type="text" cols="10" rows="5" class="form-control mt-1" id="deskripsiProduk" placeholder="Masukkan Deskripsi Produk" name="deskripsi" required></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label mb-1" for="gambar">Gambar</label>
                                        <input type="file" class="form-control mt-1" id="gambar" placeholder="Masukkan Rating Produk" name="gambar" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="box">
                        <div class="box-body">
                            <div class="col-lg-12 text-end">
                                <a type="button" href="{{ url('/admin/produk') }}" class="btn btn-secondary">Kembali</a>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
          </form>
        </div>
      </div>
      <!-- END Dynamic Table Full -->
      <!-- Dynamic Table Responsive -->
    </div>
    <!-- END Page Content -->
  </main>

@endsection