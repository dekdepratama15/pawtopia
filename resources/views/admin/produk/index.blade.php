@extends('layouts.admin.app')

@section('content')

<main id="main-container">
    <!-- Hero -->
    <div class="bg-body-light">
      <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
          <h1 class="flex-grow-1 fs-3 fw-semibold my-2 my-sm-3">Produk</h1>
        </div>
      </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
      <!-- Dynamic Table Full -->
      <div class="block block-rounded">
        <div class="block-header row pt-4">
            <div class="col-lg-6">
                <h3 class="block-title">
                    Data <b>Produk</b>
                </h3>
            </div>
            <div class="col-lg-6 text-end">
                <a type="button" href="{{url('/admin/produk/create')}}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Produk</a>
            </div>
        </div>
        <div class="block-content block-content-full">
          <table class="table table-bordered table-striped table-vcenter data-table">
            <thead>
              <tr>
                <th>Gambar</th>
                <th>Nama Produk</th>
                <th>Merek</th>
                <th>Kategori</th>
                <th>Stok</th>
                <th>Harga</th>
                <th width="25%" class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($produk as $item)
            <tr>
                <td class="text-center align-items-center" ><img style="max-width: 150px;" src="{{url('upload/'.$item->gambar)}}" alt=""></td>
                <td>{{$item->nama_produk}}</td>
                <td>{{$item->merk->nama_merk}}</td>
                <td>{{$item->kategori->nama_kategori}}</td>
                <td>{{$item->stok}}</td>
                <td>Rp. {{number_format($item->harga, 0, '', '.')}}</td>
                <td class="text-center">
                    <a type="button" href="{{route('produk.show', $item->id)}}" class="btn btn-warning btn-sm"><i class="fa fa-eye"></i> Detail</a>
                    <a type="button" href="{{route('produk.edit', $item->id)}}" class="btn btn-info btn-sm"><i class="fa fa-pen"></i> Edit</a>
                    <button type="button" class="btn btn-sm btn-danger delete-data" data-redirect="produk" data-id="{{ $item->id }}" data-token="{{ csrf_token() }}"><i class="fa fa-trash"></i> Delete</button>
                </td>
            </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <!-- END Dynamic Table Full -->
    </div>
    <!-- END Page Content -->
  </main>

@endsection