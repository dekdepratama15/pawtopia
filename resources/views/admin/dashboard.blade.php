@extends('layouts.admin.app')

@section('content')
<main id="main-container">
    <!-- Hero -->
    <div class="content">
      <div class="d-md-flex justify-content-md-between align-items-md-center py-3 pt-md-3 pb-md-0 text-center text-md-start">
        <div>
          <h1 class="h3 mb-1">
            Dashboard
          </h1>
          <p class="fw-medium mb-0 text-muted">
            Welcome, Superadmin!
          </p>
        </div>
      </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
      <!-- Overview -->
      <div class="row items-push">
        <div class="col-sm-6 col-xl-4">
          <div class="block block-rounded text-center d-flex flex-column h-100 mb-0">
            <div class="block-content block-content-full flex-grow-1">
              <div class="item rounded-3 bg-body mx-auto my-3">
                <i class="fa fa-users fa-lg text-primary"></i>
              </div>
              <div class="fs-1 fw-bold">{{count($user)}}</div>
              <div class="text-muted mb-3">User Yang Terdaftar</div>
            </div>
            <div class="block-content block-content-full block-content-sm bg-body-light fs-sm">
              <a class="fw-medium" href="{{url('/admin/user')}}">
                Semua User
                <i class="fa fa-arrow-right ms-1 opacity-25"></i>
              </a>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-xl-4">
          <div class="block block-rounded text-center d-flex flex-column h-100 mb-0">
            <div class="block-content block-content-full flex-grow-1">
              <div class="item rounded-3 bg-body mx-auto my-3">
                <i class="fa fa-level-up-alt fa-lg text-primary"></i>
              </div>
              <div class="fs-1 fw-bold">{{count($produk)}}</div>
              <div class="text-muted mb-3">Produk</div>
            </div>
            <div class="block-content block-content-full block-content-sm bg-body-light fs-sm">
              <a class="fw-medium" href="{{url('/admin/produk')}}">
                Semua Produk
                <i class="fa fa-arrow-right ms-1 opacity-25"></i>
              </a>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-xl-4">
          <div class="block block-rounded text-center d-flex flex-column h-100 mb-0">
            <div class="block-content block-content-full">
              <div class="item rounded-3 bg-body mx-auto my-3">
                <i class="fa fa-wallet fa-lg text-primary"></i>
              </div>
              <div class="fs-2 fw-bold">Rp. {{number_format($total_order, 0, '', '.')}}</div>
              <div class="text-muted mb-3 mt-2">Total Pendapatan</div>
            </div>
            <div class="block-content block-content-full block-content-sm bg-body-light fs-sm mt-2">
              <a class="fw-medium" href="{{url('/admin/transaksi')}}">
                Semua Transaksi
                <i class="fa fa-arrow-right ms-1 opacity-25"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
      <!-- END Overview -->

      <!-- Latest Orders + Stats -->
      <div class="row">
        <div class="col-md-8">
          <!--  Latest Orders -->
          <div class="block block-rounded block-mode-loading-refresh">
            <div class="block-header block-header-default">
              <h3 class="block-title">
                Penjualan
              </h3>
            </div>
            <div class="block-content">
              <canvas id="chLine"></canvas>
            </div>
            <div class="block-content block-content-full block-content-sm bg-body-light fs-sm text-center">
              <!-- <a class="fw-medium" href="{{url('/admin/transaksi')}}">
                Lihat Transaksi
                <i class="fa fa-arrow-right ms-1 opacity-25"></i>
              </a> -->
              <a href="{{route('transaksi.export')}}" type="button" class="btn btn-sm btn-success">
                  <i class="fa fa-file-excel" aria-hidden="true"></i> Export Excel
              </a>
            </div>
          </div>
          <!-- END Latest Orders -->
        </div>
        <div class="col-md-4 d-flex flex-column">
          <!-- Stats -->
          <div class="block block-rounded">
            <div class="block-content block-content-full d-flex justify-content-between align-items-center flex-grow-1">
              <div class="me-3">
                <p class="fs-3 fw-bold mb-0">
                    {{count($transaksi_done)}}
                </p>
                <p class="text-muted mb-0">
                  Completed orders
                </p>
              </div>
              <div class="item rounded-circle bg-body">
                <i class="fa fa-check fa-lg text-primary"></i>
              </div>
            </div>
            <div class="block-content block-content-full block-content-sm bg-body-light fs-sm text-center">
              <a class="fw-medium" href="{{url('/admin/transaksi')}}">
                Lihat Transaksi
                <i class="fa fa-arrow-right ms-1 opacity-25"></i>
              </a>
            </div>
          </div>
          <div class="block block-rounded text-center d-flex flex-column flex-grow-1">
            <div class="block-content block-content-full d-flex align-items-center flex-grow-1">
              <div class="w-100">
                <div class="item rounded-3 bg-body mx-auto my-3">
                  <i class="fa fa-archive fa-lg text-primary"></i>
                </div>
                <div class="fs-1 fw-bold">{{count($sisa_produk)}}</div>
                <div class="text-muted mb-3">Produk yang hampir habis</div>
              </div>
            </div>
            <div class="block-content block-content-full block-content-sm bg-body-light fs-sm">
              <a class="fw-medium" href="{{url('/admin/produk')}}">
                Lihat Produk
                <i class="fa fa-arrow-right ms-1 opacity-25"></i>
              </a>
            </div>
          </div>
          <!-- END Stats -->
        </div>
      </div>
      <!-- END Latest Orders + Stats -->
    </div>
    <!-- END Page Content -->
  </main>
@endsection

@section('js-extra')
<script>
  $(document).ready(function () {
    /* chart.js chart examples */
    var groupedData = [];
    $.each(JSON.parse('{!! $groupedData !!}'), function(key, val) {
      groupedData.push(val.length);
    })
    // chart colors
    var colors = ['#007bff','#28a745','#333333','#c3e6cb','#dc3545','#6c757d'];

    /* large line chart */
    var chLine = document.getElementById("chLine");
    var chartData = {
      labels: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"],
      datasets: [{
        data: groupedData,
        backgroundColor: 'transparent',
        borderColor: colors[0],
        borderWidth: 4,
        pointBackgroundColor: colors[0]
      }
      ]
    };
    if (chLine) {
      new Chart(chLine, {
      type: 'line',
      data: chartData,
      options: {
        scales: {
          xAxes: [{
            ticks: {
              beginAtZero: false
            }
          }]
        },
        legend: {
          display: false
        },
        responsive: true
      }
      });
    }
  })
</script>
@endsection
